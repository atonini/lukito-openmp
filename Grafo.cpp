#include <cstdlib>
#include <iostream>
#include <omp.h>
#include <cstdio>
#include "Grafo.hpp"


using namespace std;


/*Este é um grafo direcionado
 * A origem do grafo é sempre a linha e o destino a coluna
 * o valor da matriz é o custo
 */
   
Grafo::Grafo(int n){
     Grafo::n=n;
     
     grafo_direcionado = new int * [n];
      
     for(int i=0; i<n; i++){
          Grafo::grafo_direcionado[i]= new int[n];          
        }
     
     for(int i=0; i<n; i++){
          for(int j=0; j<n; j++){
               Grafo::grafo_direcionado[i][j]=-1;

           }
      }
}

Grafo::~Grafo(){
    delete[] grafo_direcionado;
}
    
    /*
     *  Método que insere os elementos no grafo
     *  Como é um grafo direcionado, a seta aponta a o destino
     *  a origem eh a linha e o destino eh a coluna, e o valor
     *  armazenado é o custo (custo não pode ser negativo!)
     *  Retorno: Retorna O se a inserção foi realizada com sucesso e 1 caso ocorra erro
     * como o custo ser negativo
     */
int Grafo::inserir_elemento(int origem, int destino, int custo){
       
        if( ( (origem || destino) > n ) || ((origem || destino) < 0 ) || ( (custo) < 0 )  )
           return 1;
       
        Grafo::grafo_direcionado[origem-1][destino-1]=custo;
        
        return 0;
        
    }
  
    /*
     * Calcula o menor caminho de uma origem para todos os nodos do grafo
     * com o algoritmo do menor caminho de Bellmann-Ford sem pesos negativos
     * a origem deve existir no grafo 
     */
    
 int Grafo::menor_caminho(int source){    
        
        if ( (source < 0) || (source > n) )
            return 1;
        
        int set[n];
        int dist[n];
        int nodo=0;
        int i=0, j=0;
        int tam=0;
        int u=0;

        #pragma omp parallel
        {
            #pragma omp parallel for
            for(int i=0 ; i<n ; i++){
                dist[i]=INFINITO;
            }
        }
        dist[source-1]=0;

        
        while(i<n){
            set[i]=0;
            i++;
        }

        set[source-1]=1;
        tam=1;
        
        while(tam>0){      
            
            i=0;
            while(set[i]!=1){
                 i++;
            } 
            
            nodo=i;
            set[i]=0;
            tam--; 
            
            #pragma omp parallel private (j,u)
            {
                #pragma omp parallel for schedule (static)
                    for( j=0; j<n; j++){ //procuro pelos vizinhos do nodo
                                                
                        if(Grafo::grafo_direcionado[nodo][j]!=-1){ //se forem vizinhos eu calculo as distancias

                            u = dist[nodo] + Grafo::grafo_direcionado[nodo][j]; //distancia atual + até o vizinho
                            if( u < dist[j] ){      //se a distancia atual + a até o vizinho for menor que a ja existente
                               #pragma omp critical
                                {     
                                    dist[j] = u;    //está se torna a menor distancia
                                    if(set[j]==0) tam++;  //verifica se o nodo ja está no conjunto
                                        set[j]=1;             // se nao esta adiciona ele  e incrementa o tamanho do conjunto   
                                }
                              }
                           }
                       
                      } //fim for
            }//fim pragma      
          } //fim while
        
        cout << "{\"Origem\":" << source << "}" << "\n";
        for(int i=0; i<n ;i++){
            if(dist[i]==INFINITO) 
                {cout << "{\"Nodo\":"<< i+1 <<" ,\"distância\":" << "não há caminho" << "}\n";
            }else{
                cout << "{\"Nodo\":"<< i+1 <<" ,\"distancia\":" << dist[i] << "}\n";
            }
        }
        
        cout <<"\n";
        
        return 0;
    } //fim metodo

 /* 
     * Imprime o grafo na tela em forma de matriz de adjacencia
     */
void Grafo::print_grafo(){
    
         cout << " " << " "; 
         for(int i=0; i<n; i++){
             cout << i+1 << "";
         }
         cout << "\n";
         
         for(int i=0; i<n; i++){
           cout << i+1 << "";
 
           for(int j=0; j<n; j++){             
              cout << Grafo::grafo_direcionado[i][j] << " ";
           }
          cout << "\n";    
    }
         
    }
   
 
