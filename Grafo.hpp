#ifndef GRAFO_H
#define GRAFO_H

#define INFINITO 99999999

using namespace std;

class Grafo{
    
    int n;
    int **grafo_direcionado;
    
public:
    
    Grafo(int n);
    ~Grafo();
    
    int inserir_elemento(int origem, int destino, int custo);
    int menor_caminho(int source);
    void print_grafo();
    
};
 
    
#endif