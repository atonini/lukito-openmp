/* 
 Autor: Aline Tonini
 Implementação do problema do Menor Caminho 
 */ 

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <omp.h>
#include <cstdio>
#include <vector>
#include <boost/algorithm/string.hpp>
#include "Grafo.hpp"

using namespace std;
using namespace boost;

/*Este é um grafo direcionado
 * A origem do grafo é sempre a linha e o destino a coluna
 * o valor da matriz é o custo
 */
   

int main(int argc, char *argv[]){
    
    int n=0, source=0;
    int num_proc;
    if (argc>1){
        n = atoi(argv[1]);
        source= atoi (argv[2]);        
    }else {
        cout << "passagem de parametros invalida!";
        return 1; 
    }
  
    num_proc=omp_get_num_procs ();
    
    if(num_proc==1){
       omp_set_num_threads(2);
    } else{
       omp_set_num_threads(num_proc); 
    }
    
    int origem, destino, custo;
    Grafo * G  = new Grafo(n);
      
    string line;
    ifstream entrada ("entrada.txt"); // ifstream = padrão ios:in
    int ret=0;
    
    if (entrada.is_open()){

        while (! entrada.eof() ){

            getline (entrada,line); 
            vector<string> str_split;
            split(str_split,line,is_any_of(" ")); 
            origem = atoi(str_split[0].c_str());
            destino= atoi(str_split[1].c_str());
            custo= atoi(str_split[2].c_str());
            
            ret=G->inserir_elemento(origem,destino,custo);
            if(ret==1){ 
                cout << "ERRO ao inserir elemento no grafo! \n" ;
                return 1;
            }
            
        }
        entrada.close();

     } else {
        cout << "Unable to open file"; 

    }
    
    G->menor_caminho(source);
    
    
    return 0;
    
}
