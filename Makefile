# Autor : Aline Tonini
# Makefile
#

CC=g++
CFLAGS=-c
LDFLAGS= -fopenmp -lboost_system -I /usr/include/boost/algorithm
SOURCES= Main.cpp Grafo.cpp
HEADS= Grafo.hpp
OBJECTS= $(SOURCES:.cpp=.o)
EXECUTABLE=exec

all: $(EXECUTABLE)	

$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) $(HEADS) -o $(EXECUTABLE)

%.o: %.cpp 
	$(CC) $(CFLAGS) $(SOURCES)

clean:
	rm -rf *o exec