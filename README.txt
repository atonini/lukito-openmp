#####################################################################################################################
#                   Lukito-OpenMP  - Trabalho I de Introduçao ao Processamento Paralelo e Distribuído
#####################################################################################################################
#
#   1. Introduçao
#   2. Implementação 
#   3. Execução
#   
#   @autor Aline Rodrigues Tonini
#   @version 1.0
# 
#####################################################################################################################
#   1. Introduçao
#####################################################################################################################
#
#   O projeto Lukito se propõem a realizar o trabalho I da disciplina de IPPD, proposto pelo Prof. Gerson Cavalheiro.
#   O trabalho proposto é a realizar a implementação do algoritmo de busca para o menor caminho Single-Source Shortest
#   Path) (http://iss.ices.utexas.edu/?p=projects/galois/benchmarks/single_source_shortest_path)
#   com uma ferramenta de programação concorrente. Neste caso foi utilizado a ferramenta OpenMP e o algoritmo usado
#   é uma variação do algoritmo de Bellmann-Ford para menores caminhos. 
#
#####################################################################################################################
#   2. Implementação
#####################################################################################################################
#
#    Na implementação do projeto foi utilizado a linguagem C++, a biblioteca boost para a leitura do arquivo
#    de entrada e a ferramenta OpenMP. O trabalho foi desenvolvido utilizando a IDE Netbeans e o versionador 
#    Bitbucket para manter o código organizado.
#
#    Na implementação foi criada uma classe grafo onde o grafo foi implementado com uma matriz de adjacencias.
#    O grafo é direcionado e o valor da matriz é o custo. Não são permitidos custos negativos nesta versão
#    do algoritmo de Bellmann-Ford. Por exemplo origem->1, destino->2, custo->5 é implementado desta forma :
#				grafo[1][2]=5
#
#    Ao ser executado o programa  é passado o número de vértices do grafo.
#    Onde 0 é o valor mínimo para o "nome" de um nodo e n é o valor máximo.
#
#    A concorrencia é implementado da seguinte forma: primeiro é verificado o número de processadores disponíveis,
#    Se tiver apenas um processador, é criado apenas 2 threads, se tiver mais processadores, o número de threads é
#    o número de processadores. São usadas as primitivas parallel, parallel for e critical. A primitiva parallel é 
#    usada para definir regiões paralelas.
#    Essas regiões são inicializações de matrizes ou vetores em geral. A primitiva parallel for é usada para 
#    a inicialização do grafo e do vetor de distâncias e também na implementação do algoritmo,
#    onde este verificada a distância do nodo atual com seus vizinhos e calcula a nova distância da origem.
#    A diretiva critical é usada no algoritmo para que não haja um acesso indevido na atualização do vetor de
#    distâncias e do conjunto de nodos que devem ser visitados.
#
#####################################################################################################################
# 	3. Execução
#####################################################################################################################
#
#	Para executar é necessário possuir o arquivo Main.cpp, Grafo.cpp, Grafo.hpp o Makefile e o arquivo entrada.txt
#	e a biblioteca Boost instalada**.
#	O arquivo entrada.txt já contém a entrada do grafo, cada linha do arquivo representa uma entrada, que está
# 	no formato "origem destino custo".
#	Por exemplo:
#           	1 2 5  origem=1 destino=2 custo=5
#
#	Após o Makefile, execute passando como parametro o número de nodos e o nodo de origem.
#	 			./exec NUM_NODOS NODO_ORIGEM
#
#	O nome do arquivo de entrada está setado no código, se mudar o nome, este deve ser alterado no código.
#
#	**Caso ocorra algum erro por seu computador não conter a biblioteca boost instalada, instale usando
#	 Fedora : yum install boost-devel
#	 Ubunto : sudo apt-get install libboost-all-dev
#
#####################################################################################################################